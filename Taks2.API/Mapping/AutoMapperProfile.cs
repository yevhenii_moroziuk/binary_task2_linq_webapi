﻿U﻿using AutoMapper;
using Taks2.API.DTOs;
using Taks2.API.Models;

namespace Taks2.API.Mapping
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			//User
			CreateMap<User, UserShowDTO>();
			CreateMap<User, UserPostDTO>();
			CreateMap<UserPostDTO, User>();
			//Team
			CreateMap<Team, TeamDTO>();
			CreateMap<TeamDTO, Team>();
			//Task
			CreateMap<Task, TaskDTO>();
			CreateMap<TaskDTO, Task>();
			//Project
			CreateMap<Project, ProjectDTO>();
			CreateMap<ProjectDTO, Project>();
		}
	}
}
