﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Taks2.API.DTOs;
using Taks2.API.Models;
using Taks2.API.Repository;

namespace Taks2.API.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class PrTaskController : ControllerBase
    {
		private IPrTaskRepository _repository;

		public PrTaskController(IPrTaskRepository taskRepository)
		{
			_repository = taskRepository;
		}

		[HttpGet("task1/{author}")]
		public IDictionary<string, int> Task1(int author)
		{
			return _repository.GetTasksCount(author);
		}

		[HttpGet("task2/{userid}")]
		public ActionResult<List<Task>> Task2(int userid)
		{
			return _repository.GetTasks(userid);
		}
		
		[HttpGet("task3/{userid}")]
		public ActionResult<List<string>> Task3(int userid)
		{
			return _repository.GetFinishedTasks(userid);
		}

		[HttpGet("task4/")]
		public IDictionary<string, List<User>> Task4()
		{
			return _repository.GetTeamDefinition();
		}

		[HttpGet("task5/")]
		public IDictionary<User, List<Task>> Task5()
		{
			return _repository.GetUserTasks();
		}
		[HttpGet("task6/{userid}")]
		public ActionResult<Task6DTO> Task6(int userid)
		{
			return _repository.GetUserDefinition(userid);
		}

		[HttpGet("task7/{projectid}")]
		public ActionResult<Task7DTO> Task7(int projectid)
		{
			return _repository.GetProjectDefinition(projectid);
		}
	}
}