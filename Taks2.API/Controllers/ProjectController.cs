﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Taks2.API.DTOs;
using Taks2.API.Models;
using Taks2.API.Repository;

namespace Taks2.API.Controllers
{
	[Route("api/projects")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
		private readonly IRepository<Project> _repository;
		private readonly IMapper _mapper;

		public ProjectController(IRepository<Project> repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}

		[HttpGet]
		public IActionResult GetProjects()
		{
			var projects = _repository.ShowAll();
			return Ok(_mapper.Map<IEnumerable<ProjectDTO>>(projects));
		}

		[HttpGet("{id}")]
		public IActionResult GetProject(int id)
		{
			if (!_repository.IsExists(id))
				return NotFound();

			var project = _repository.Show(id);

			return Ok(_mapper.Map<ProjectDTO>(project));
		}

		[HttpPost]
		public IActionResult PostProject(ProjectDTO projectDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (_repository.IsExists(projectDTO.Id))
				return BadRequest();

			var project = _mapper.Map<Project>(projectDTO);

			try
			{
				_repository.Create(project);
				return Created("api/project", project);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid AuthorId or TeamId");
			}
		}

		[HttpPut]
		public IActionResult EditProject(Project entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid project");

			if (!_repository.IsExists(entity.Id))
				return NotFound();

			try
			{
				_repository.Update(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid AuthorId or TeamId");
			}
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteTask(int id)
		{
			if (!_repository.IsExists(id))
				return BadRequest();

			_repository.Delete(id);

			return Ok("deleted");
		}
	}
}