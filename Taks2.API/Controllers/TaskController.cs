﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Taks2.API.DTOs;
using Taks2.API.Models;
using Taks2.API.Repository;

namespace Taks2.API.Controllers
{
	[Route("api/tasks")]
    [ApiController]
    public class TaskController : ControllerBase
    {
		private readonly IRepository<Task> _repository;
		private readonly IMapper _mapper;

		public TaskController(IRepository<Task> repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}

		[HttpGet]
		public IActionResult GetTasks()
		{
			var tasks = _repository.ShowAll();
			return Ok(_mapper.Map<IEnumerable<TaskDTO>>(tasks));
		}

		[HttpGet("{id}")]
		public IActionResult GetTask(int id)
		{
			if (!_repository.IsExists(id))
				return NotFound();

			var task = _repository.Show(id);

			return Ok(_mapper.Map<TaskDTO>(task));
		}

		[HttpPost]
		public IActionResult PostTask(TaskDTO taskDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (_repository.IsExists(taskDTO.Id))
				return BadRequest();

			var task = _mapper.Map<Task>(taskDTO);

			try
			{
				_repository.Create(task);
				return Created("api/task", task);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid project or performer id");
			}
		}

		[HttpPut]
		public IActionResult EditTask(Task entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid user id");

			if (!_repository.IsExists(entity.Id))
				return NotFound();

			try
			{
				_repository.Update(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid team id");
			}
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteTask(int id)
		{
			if (!_repository.IsExists(id))
				return BadRequest();

			_repository.Delete(id);

			return Ok("deleted");
		}
	}
}