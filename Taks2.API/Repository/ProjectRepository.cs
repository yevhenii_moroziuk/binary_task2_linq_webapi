﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Taks2.API.Data;
using Taks2.API.Models;

namespace Taks2.API.Repository
{
	public class ProjectRepository : IRepository<Project>
	{
		private readonly IDataSource _context;
		public ProjectRepository(IDataSource source)
		{
			_context = source;
		}
		public void Create(Project entity)
		{
			//Project has User & Team FK
			if (_context.Users.Find(u => u.Id == entity.Author_Id) == null ||
				_context.Teams.Find(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid AuthorId or TeamId");

			_context.Projects.Add(entity);
		}

		public void Delete(int id)
		{
			var project = _context.Projects.Find(p => p.Id == id);
			_context.Projects.Remove(project);
		}

		public bool IsExists(int id)
		{
			return _context.Projects.SingleOrDefault(p => p.Id == id) == null ? false : true;
		}

		public Project Show(int id)
		{
			return _context.Projects.SingleOrDefault(p => p.Id == id);
		}

		public List<Project> ShowAll()
		{
			return _context.Projects;
		}

		public void Update(Project entity)
		{
			//Project has User & Team FK
			if (_context.Users.Find(u => u.Id == entity.Author_Id) == null ||
				_context.Teams.Find(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid AuthorId or TeamId");

			var project = _context.Projects.SingleOrDefault(t => t.Id == entity.Id);
			project = entity;
		}
	}
}
