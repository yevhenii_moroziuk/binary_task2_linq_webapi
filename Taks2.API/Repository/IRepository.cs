﻿using System.Collections.Generic;

namespace Taks2.API.Repository
{
	public interface IRepository<TEntity>
	{
		void Create(TEntity entity);
		void Update(TEntity entity);
		void Delete(int id);
		List<TEntity> ShowAll();
		TEntity Show(int id);
		bool IsExists(int id);
	}
}
