﻿using System;
using System.Collections.Generic;
using System.Linq;
using Taks2.API.Data;
using Taks2.API.Models;

namespace Taks2.API.Repository
{
	public class TaskRepository : IRepository<Task>
	{
		private readonly IDataSource _context;
		public TaskRepository(IDataSource source)
		{
			_context = source;
		}
		public void Create(Task entity)
		{
			//Task has Project & Team FK
			if ( _context.Users.Find(u=>u.Id == entity.Performer_Id) == null ||
				_context.Projects.Find(p => p.Id == entity.Project_Id) == null)
				throw new Exception("Invalid PerformerId or ProjectID");

			_context.Tasks.Add(entity);
		}

		public void Delete(int id)
		{
			var task = _context.Tasks.Find(t => t.Id == id);
			_context.Tasks.Remove(task);
		}

		public bool IsExists(int id)
		{
			return _context.Tasks.SingleOrDefault(u => u.Id == id) == null ? false : true;
		}

		public Task Show(int id)
		{
			return _context.Tasks.SingleOrDefault(t => t.Id == id);
		}

		public List<Task> ShowAll()
		{
			return _context.Tasks;
		}

		public void Update(Task entity)
		{
			//Task has Project & Performer FK
			if (_context.Users.Find(u => u.Id == entity.Performer_Id) == null ||
				_context.Projects.Find(p => p.Id == entity.Project_Id) == null)
				throw new Exception("Invalid PerformerId or ProjectID");

			var task = _context.Tasks.SingleOrDefault(t => t.Id == entity.Id);
			task = entity;
		}
	}
}
