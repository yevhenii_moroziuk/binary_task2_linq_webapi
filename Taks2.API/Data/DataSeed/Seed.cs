﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Taks2.API.Data.DataSeed
{
	public class Seed
	{
		public static List<T> SeedData<T>(string filename)
		{
			var parsedData = System.IO.File.ReadAllText($"Data/DataSeed/_{filename}.json");
			return JsonConvert.DeserializeObject<List<T>>(parsedData);
		}
	}
}
