﻿using System;
using System.Collections.Generic;
using System.Linq;
using Taks2.API.Models;

namespace Taks2.API.Data
{
	public interface IDataSource
	{
		List<Project> Projects { get; set; }
		List<User> Users { get; set; }
		List<Task> Tasks { get; set; }
		List<Team> Teams { get; set; }
	}
}
