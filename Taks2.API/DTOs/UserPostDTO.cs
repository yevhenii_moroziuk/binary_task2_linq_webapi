﻿using System;

namespace Taks2.API.DTOs
{
	public class UserPostDTO
	{
		public int Id { get; set; }
		public string First_Name { get; set; }
		public string Last_Name { get; set; }
		public string Email { get; set; }
		public DateTime Birthday { get; set; }
		public int? Team_Id { get; set; }
	}
}
