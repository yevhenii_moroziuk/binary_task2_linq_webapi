﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Taks2.API.DTOs
{
	public class ProjectDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime Created_At { get; set; }
		public int? Author_Id { get; set; }
		public int? Team_Id { get; set; }
	}
}
