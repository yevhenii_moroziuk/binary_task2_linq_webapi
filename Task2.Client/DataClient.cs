﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Task2.Client
{
	class DataClient
	{
		private readonly HttpClient _client;
		private readonly string _baseAddress = "https://localhost:44348/api/prtask/";
		public DataClient()
		{
			_client = new HttpClient();
		}

		public async Task<T> GetAsync<T>(string specificAddres)
		{
			var ans = await _client.GetStringAsync(_baseAddress + specificAddres);
			return JsonConvert.DeserializeObject<T>(ans);
		}
		
	}
}
