﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task2.Client.Helpers;
using Task2.Client.Models;
using Task = System.Threading.Tasks.Task;

namespace Task2.Client
{
	class Program
	{
		static DataClient dataClient = new DataClient();

		static async Task Main(string[] args)
		{


			//var ans = await client.GetStringAsync("https://localhost:44348/api/prtask/task1/50");
			//var t = JsonConvert.DeserializeObject<Dictionary<string,int>>(ans);

			await Task1Async(50);
			await Task2Async(30);
			await Task3Async(50);
			await Task4Async();
			await Task5Async();
			await Task6Async(50);
			await Task7Async(7);

			Console.ReadKey();
		}

		public static async Task Task1Async(int id)
		{
			Helper.TaskDefinition(1);
			var task2 = await dataClient.GetAsync<Dictionary<string, int>>($"task1/{id}");
			foreach (var item in task2)
			{
				Console.WriteLine($"{item.Key}   {item.Value}");
			}

			Console.WriteLine("================================");
		}

		public static async Task Task2Async(int id)
		{
			Helper.TaskDefinition(2);
			var task1 = await dataClient.GetAsync<List<Models.Task>>($"task2/{id}");

			foreach (var item in task1)
			{
				Console.WriteLine($"{item.ToString()}");
			}

			Console.WriteLine("================================");
		}

		public static async Task Task3Async(int id)
		{
			Helper.TaskDefinition(3);
			var task3 = await dataClient.GetAsync<List<string>>($"task3/{id}");

			foreach (var item in task3)
			{
				Console.WriteLine(item);
			}

			Console.WriteLine("================================");
		}

		public static async Task Task4Async()
		{
			Helper.TaskDefinition(4);
			var task4 = await dataClient.GetAsync<IDictionary<string, List<User>>>("task4");

			foreach (var item in task4)
			{
				Console.WriteLine(item.Key);
				foreach (var i in item.Value)
				{
					Console.WriteLine(i.First_Name);
				}
			}

			Console.WriteLine("================================");
		}

		public static async Task Task5Async()
		{
			Helper.TaskDefinition(5);
			var task5 = await dataClient.GetAsync<IDictionary<string, List<Models.Task>>>("task5");

			foreach (var item in task5)
			{
				//Console.WriteLine("User name: " + item.Key.First_Name);
				Console.BackgroundColor = ConsoleColor.DarkGray;
				Console.WriteLine(item.Key);
				Console.ResetColor();
				foreach (var task in item.Value)
				{
					Console.WriteLine("	Task name: " + task.Name);
					//Console.WriteLine(i.ToString());
				}
			}

			Console.WriteLine("================================");
		}

		public static async Task Task6Async(int id)
		{
			Helper.TaskDefinition(6);
			var task6 = await dataClient.GetAsync<Task6>($"task6/{id}");	

			Console.WriteLine("User: " + task6.User.ToString());
			Console.WriteLine("Last project: " + task6.LastProject.ToString());
			Console.WriteLine("Count of all tasks in the last project: " + task6.TaskCount);
			Console.WriteLine("Count of cancled/uncomplited tasks in the last project: " + task6.UnfinishedTaskCount);
			Console.WriteLine("Longest task: " + task6.LongestTask.ToString());

			Console.WriteLine("================================");
		}

		public static async Task Task7Async(int id)
		{
			Helper.TaskDefinition(7);
			var task7 = await dataClient.GetAsync<Task7>($"task7/{id}");
			Console.WriteLine("Project: " + task7.Project.ToString());
			Console.WriteLine("Longest project`s task by definition: " + task7.TaskByDescription.ToString());
			Console.WriteLine("Shortest project`s task by name: " + task7.TaskByLength.ToString());
			Console.WriteLine("Count of users in current project (0 - if do not satisfy conditions)" + task7.CountOfUsers);

			Console.WriteLine("================================");
		}
	}
}
