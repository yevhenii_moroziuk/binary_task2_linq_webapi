﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2.Client.Models
{
	public enum TaskState
	{
		Created = 0,
		Started = 1,
		Finished = 2,
		Canceled = 3
	}
}
